
import java.util

import org.opencv.core._
import org.opencv.highgui.Highgui
import org.opencv.imgproc.Imgproc

import reflect._
import scala.collection.mutable

object Test extends App {
  def run() {
    println(s"\nRunning ${classTag[this.type].toString.replace("$", "")}")

    val inputImage = Highgui.imread(getClass.getResource("/AverageMaleFace.jpg").getPath, Highgui.CV_LOAD_IMAGE_COLOR)
    val skinImage = inputImage.clone()

    val hlsImg = new Mat()
    val rgbImg = new Mat()

    Imgproc.cvtColor(inputImage, hlsImg, Imgproc.COLOR_BGR2HLS)
    Imgproc.cvtColor(inputImage, rgbImg, Imgproc.COLOR_BGR2RGB)

    val hlsImages = new util.ArrayList[Mat](3)
    Core.split(hlsImg, hlsImages)

    val rMap = mutable.Map.empty[Double, Int]
    val gMap = mutable.Map.empty[Double, Int]
    val bMap = mutable.Map.empty[Double, Int]

    for (row <- 0 until hlsImg.rows()) {
      for (col <- 0 until hlsImg.cols()) {
        val h = hlsImg.get(row, col)(0)
        val l = hlsImg.get(row, col)(1)
        val s = hlsImg.get(row, col)(2)

        val lsRatio = l / s
        val skinPixel = (s >= 50) && (lsRatio > 0.5) && (lsRatio < 3.0) && ((h <= 14) || (h >= 165))
        if (skinPixel) {
          val r = rgbImg.get(row, col)(0)
          val g = rgbImg.get(row, col)(1)
          val b = rgbImg.get(row, col)(2)

          if (!rMap.contains(r)) {
            rMap(r) = 0
          }
          rMap(r) += 1

          if (!gMap.contains(g)) {
            gMap(g) = 0
          }
          gMap(g) += 1

          if (!bMap.contains(b)) {
            bMap(b) = 0
          }
          bMap(b) += 1

          println(r + "/" + g + "/" + b)
        } else { // skin 이 아니면 검정색으로 표시...
          skinImage.put(row, col, Array[Byte](0, 0, 0))
        }
      }
    }

    println(rMap.toList.sortBy(_._2).last)
    println(gMap.toList.sortBy(_._2).last)
    println(bMap.toList.sortBy(_._2).last)

    val filename = "scalaFaceDetection.png"
    assert(Highgui.imwrite(filename, skinImage))
  }

  run()
}