import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._

packageArchetype.java_server

name := "makeupperfect-worker"

version := "1.0"

mainClass in Compile := Some("Test")

scalacOptions ++= Seq(
  "-deprecation", "-encoding", "UTF-8", "-feature",
  "-target:jvm-1.7", "-unchecked", "-Ywarn-adapted-args",
  "-Ywarn-value-discard")

javacOptions ++= Seq("-source", "1.7", "-target", "1.7", "-g:vars")

scalaVersion := "2.11.8"
